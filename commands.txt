#####
##Add user and vhosts
#####
rabbitmqctl add_user USER_NAME
rabbitmqctl add_vhost middleware
rabbitmqctl set_permissions -p middleware USER_NAME "." "." ".*"

#####
##Cluster settings
#####
rabbitmqctl stop_app
rabbitmqctl reset
rabbitmqctl join_cluster rabbit@rabbit_node
